desert_music = {}

local musicpath = minetest.get_modpath("desert_music").."/music/"

desert_music.custom_check = function(p_name)

        local player = minetest.get_player_by_name(p_name)
        if not player then return false end
        local pos = player:get_pos()
        if minetest.find_node_near(pos, 10, {"group:sand","default:desert_stone","default:dirt_with_dry_grass","default:dry_dirt_with_dry_grass"}, true) and not minetest.find_node_near(pos, 10, {"default:grass_1","default:grass_2","default:grass_3"}, true) then
                return true
        else
                return false
        end

end

dbgm.register_music({

        file = musicpath.."the_desert.ogg",
        name = "the_desert",
        length = 130, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        custom_check = desert_music.custom_check,
})


dbgm.register_music({

        file = musicpath.."the_desert.ogg",
        name = "the_desert",
        length = 130, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        custom_check = desert_music.custom_check,
})


dbgm.register_music({

        file = musicpath.."gunslinger.ogg",
        name = "gunslinger",
        length = 101, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        custom_check = desert_music.custom_check,
})


dbgm.register_music({

        file = musicpath.."smoking_gun.ogg",
        name = "smoking_gun",
        length = 188, --length of music in sec
        author = "Kevin MacLeod",
        license = "Attribution 3.0 (CC BY 3.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        custom_check = desert_music.custom_check,
})



dbgm.register_music({

        file = musicpath.."soaring_hawk.ogg",
        name = "soaring_hawk",
        length = 201, --length of music in sec
        author = "John Bartmann",
        license = "Creative Commons BY Attribution 4.0",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        custom_check = desert_music.custom_check,

})


dbgm.register_music({

        file = musicpath.."western.ogg",
        name = "western",
        length = 172, --length of music in sec
        author = "linxiaoma",
        license = "Creative Commons BY Attribution 4.0",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        custom_check = desert_music.custom_check,
        
})



dbgm.register_music({

        file = musicpath.."western_mood.ogg",
        name = "western_mood",
        length = 172, --length of music in sec
        author = "shalpin",
        license = "Attribution 3.0 (CC BY 3.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        custom_check = desert_music.custom_check,
        
})



dbgm.register_music({

        file = musicpath.."un_desert.ogg",
        name = "un_desert",
        length = 249, --length of music in sec
        author = "Loyalty Freak Music",
        license = "CC0",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        custom_check = desert_music.custom_check,
        
})