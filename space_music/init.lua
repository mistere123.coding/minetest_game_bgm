space_music = {}
space_music.min_height = 1000
space_music.max_height = 31000

local musicpath = minetest.get_modpath("space_music").."/music/"


dbgm.register_music({

        file = musicpath.."there_is_something_out_in_space.ogg",
        name = "there_is_something_out_in_space",
        length = 186, --length of music in sec
        author = "Dark Reaven",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=space_music.min_height,z=-31000},
        max_pos = {x=31000,y=space_music.max_height,z=31000},
})


dbgm.register_music({

        file = musicpath.."the_big_bang.ogg",
        name = "the_big_bang",
        length = 105, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=space_music.min_height,z=-31000},
        max_pos = {x=31000,y=space_music.max_height,z=31000},
})

dbgm.register_music({

        file = musicpath.."deep_space.ogg",
        name = "deep_space",
        length = 126, --length of music in sec
        author = "Jason Shaw",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=space_music.min_height,z=-31000},
        max_pos = {x=31000,y=space_music.max_height,z=31000},
})


dbgm.register_music({

        file = musicpath.."horror_drone_1.ogg",
        name = "horror_drone_1",
        length = 240, --length of music in sec
        author = "Jason Shaw",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=space_music.min_height,z=-31000},
        max_pos = {x=31000,y=space_music.max_height,z=31000},

})


dbgm.register_music({

        file = musicpath.."alien_sunset.ogg",
        name = "alien_sunset",
        length = 136, --length of music in sec
        author = "Jason Shaw",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=space_music.min_height,z=-31000},
        max_pos = {x=31000,y=space_music.max_height,z=31000},

})

