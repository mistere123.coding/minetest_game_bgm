sea_music = {}

local musicpath = minetest.get_modpath("sea_music").."/music/"


sea_music.custom_check = function(p_name)
        local player = minetest.get_player_by_name(p_name)
        if player then
                local pos = player:get_pos()
                if minetest.find_node_near(pos, 4, {"group:cracky"}, true) then
                        return false
                else 
                        return true 
                end
        else
                return false
        end

end,

dbgm.register_music({

        file = musicpath.."the_wellerman.ogg",
        name = "the_wellerman",
        length = 116, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-30,z=-31000},
        max_pos = {x=31000,y=20,z=31000},
        near_nodes = {"default:water_source"}, -- a list of nodes that must be near the player, accepts groups
        custom_check = sea_music.custom_check,
})


dbgm.register_music({

        file = musicpath.."seven_seas.ogg",
        name = "seven_seas",
        length = 116, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-30,z=-31000},
        max_pos = {x=31000,y=20,z=31000},
        near_nodes = {"default:water_source"}, -- a list of nodes that must be near the player, accepts groups
        --custom_check = sea_music.custom_check,
})


dbgm.register_music({

        file = musicpath.."pirates_of_the_quarantine.ogg",
        name = "pirates_of_the_quarantine",
        length = 140, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-30,z=-31000},
        max_pos = {x=31000,y=20,z=31000},
        near_nodes = {"default:water_source"}, -- a list of nodes that must be near the player, accepts groups
        --custom_check = sea_music.custom_check,
})



dbgm.register_music({

        file = musicpath.."jolly_good_captian.ogg",
        name = "jolly_good_captian",
        length = 140, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-30,z=-31000},
        max_pos = {x=31000,y=20,z=31000},
        near_nodes = {"default:water_source"}, -- a list of nodes that must be near the player, accepts groups
        custom_check = sea_music.custom_check,
})



dbgm.register_music({

        file = musicpath.."land_of_pirates.ogg",
        name = "land_of_pirates",
        length = 114, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-30,z=-31000},
        max_pos = {x=31000,y=20,z=31000},
        near_nodes = {"default:water_source"}, -- a list of nodes that must be near the player, accepts groups
        custom_check = sea_music.custom_check,
})



dbgm.register_music({

        file = musicpath.."before_the_storm.ogg",
        name = "before_the_storm",
        length = 191, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-30,z=-31000},
        max_pos = {x=31000,y=20,z=31000},
        near_nodes = {"default:water_source"}, -- a list of nodes that must be near the player, accepts groups
        --custom_check = sea_music.custom_check,
})
