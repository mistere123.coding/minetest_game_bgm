grassland_music = {}

local musicpath = minetest.get_modpath("grassland_music").."/music/"



dbgm.register_music({

        file = musicpath.."sunset_fields.ogg",
        name = "sunset_fields",
        length = 144, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        near_nodes = {"default:dirt_with_grass","default:grass_1","default:grass_2","default:grass_3"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,
})


dbgm.register_music({

        file = musicpath.."crescendo.ogg",
        name = "crescendo",
        length = 161, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        near_nodes = {"default:dirt_with_grass","default:grass_1","default:grass_2","default:grass_3"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,
})


dbgm.register_music({

        file = musicpath.."superepic.ogg",
        name = "superepic",
        length = 44, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        near_nodes = {"default:dirt_with_grass","default:grass_1","default:grass_2","default:grass_3"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,
})



dbgm.register_music({

        file = musicpath.."explorer.ogg",
        name = "explorer",
        length = 212, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        near_nodes = {"default:dirt_with_grass","default:grass_1","default:grass_2","default:grass_3"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,

})



dbgm.register_music({

        file = musicpath.."explorer.ogg",
        name = "explorer",
        length = 212, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        near_nodes = {"default:dirt_with_grass","default:grass_1","default:grass_2","default:grass_3"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,

})




dbgm.register_music({

        file = musicpath.."crossroads.ogg",
        name = "crossroads",
        length = 150, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        near_nodes = {"default:dirt_with_grass","default:grass_1","default:grass_2","default:grass_3"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,

})



dbgm.register_music({

        file = musicpath.."outro.ogg",
        name = "outro",
        length = 107, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        near_nodes = {"default:dirt_with_grass","default:grass_1","default:grass_2","default:grass_3"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,

})