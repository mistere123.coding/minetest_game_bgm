cave_music = {}
cave_music.min_height = -16500
cave_music.max_height = -100

local musicpath = minetest.get_modpath("cave_music").."/music/"

dbgm.register_music({

        file = musicpath.."intense_suspense.ogg",
        name = "intense_suspense",
        length = 93, --length of music in sec
        author = "Jason Shaw",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=cave_music.min_height,z=-31000},
        max_pos = {x=31000,y=cave_music.max_height,z=31000},
})



dbgm.register_music({

        file = musicpath.."middle_earth.ogg",
        name = "middle_earth",
        length = 117, --length of music in sec
        author = "Jason Shaw",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=cave_music.min_height,z=-31000},
        max_pos = {x=31000,y=cave_music.max_height,z=31000},
})



dbgm.register_music({

        file = musicpath.."tempting_fate.ogg",
        name = "tempting_fate",
        length = 219, --length of music in sec
        author = "Jason Shaw",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=cave_music.min_height,z=-31000},
        max_pos = {x=31000,y=cave_music.max_height,z=31000},
})



dbgm.register_music({

        file = musicpath.."the_great_unknown.ogg",
        name = "the_great_unknown",
        length = 219, --length of music in sec
        author = "Jason Shaw",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=cave_music.min_height,z=-31000},
        max_pos = {x=31000,y=cave_music.max_height,z=31000},
})




dbgm.register_music({

        file = musicpath.."chase.ogg",
        name = "chase",
        length = 129, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=cave_music.min_height,z=-31000},
        max_pos = {x=31000,y=cave_music.max_height,z=31000},
})




dbgm.register_music({

        file = musicpath.."the_wandering_king.ogg",
        name = "the_wandering_king",
        length = 164, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=cave_music.min_height,z=-31000},
        max_pos = {x=31000,y=cave_music.max_height,z=31000},
})


dbgm.register_music({

        file = musicpath.."legends.ogg",
        name = "legends",
        length = 174, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=cave_music.min_height,z=-31000},
        max_pos = {x=31000,y=cave_music.max_height + 300,z=31000},
})



dbgm.register_music({

        file = musicpath.."cockroaches.ogg",
        name = "cockroaches",
        length = 209, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=cave_music.min_height,z=-31000},
        max_pos = {x=31000,y=cave_music.max_height + 300,z=31000},
})


