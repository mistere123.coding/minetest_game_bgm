grassland_music = {}

local musicpath = minetest.get_modpath("skylands_music").."/music/"


dbgm.register_music({

        file = musicpath.."dawn.ogg",
        name = "dawn",
        length = 240, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=100,z=-31000},
        max_pos = {x=31000,y=500,z=31000},
})


dbgm.register_music({

        file = musicpath.."dream.ogg",
        name = "dream",
        length = 105, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=100,z=-31000},
        max_pos = {x=31000,y=500,z=31000},
})

dbgm.register_music({

        file = musicpath.."it_can_only_go_up.ogg",
        name = "it_can_only_go_up",
        length = 126, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=100,z=-31000},
        max_pos = {x=31000,y=500,z=31000},
})


dbgm.register_music({

        file = musicpath.."for_the_last_time.ogg",
        name = "for_the_last_time",
        length = 240, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=100,z=-31000},
        max_pos = {x=31000,y=500,z=31000},
})
