glacier_snow_music = {}

local musicpath = minetest.get_modpath("glacier_snow_music").."/music/"



dbgm.register_music({

        file = musicpath.."cold_journey.ogg",
        name = "cold_journey",
        length = 311, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=700,z=31000},
        near_nodes = {"default:ice","default:snowblock","default:snow"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,
})



dbgm.register_music({

        file = musicpath.."nomadic_dawn.ogg",
        name = "nomadic_dawn",
        length = 309, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=700,z=31000},
        near_nodes = {"default:ice","default:snowblock","default:snow"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,
})



dbgm.register_music({

        file = musicpath.."vetur_frosti.ogg",
        name = "vetur_frosti",
        length = 203, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=700,z=31000},
        near_nodes = {"default:ice","default:snowblock","default:snow"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,
})




dbgm.register_music({

        file = musicpath.."goda_nott.ogg",
        name = "goda_nott",
        length = 225, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=700,z=31000},
        near_nodes = {"default:ice","default:snowblock"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,
})




dbgm.register_music({

        file = musicpath.."mjoi.ogg",
        name = "mjoi",
        length = 209, --length of music in sec
        author = "Alexander Nakarada",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=700,z=31000},
        near_nodes = {"default:ice","default:snowblock","default:snow"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,
})




dbgm.register_music({

        file = musicpath.."carol_of_the_bells.ogg",
        name = "carol_of_the_bells",
        length = 94, --length of music in sec
        author = "audionautix.com",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=700,z=31000},
        near_nodes = {"default:ice"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,

})
