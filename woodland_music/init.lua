woodland_music = {}

local musicpath = minetest.get_modpath("woodland_music").."/music/"


dbgm.register_music({

        file = musicpath.."the_magic_of_the_forest.ogg",
        name = "the_magic_of_the_forest",
        length = 170, --length of music in sec
        author = "Dark Reaven",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        near_nodes = {"group:tree"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,
})

dbgm.register_music({

        file = musicpath.."cube_waltz.ogg",
        name = "cube_waltz",
        length = 118, --length of music in sec
        author = "Dark Reaven",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        near_nodes = {"group:tree"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,
})


dbgm.register_music({

        file = musicpath.."calmed_cube.ogg",
        name = "calmed_cube",
        length = 144, --length of music in sec
        author = "Dark Reaven",
        license = "Attribution 4.0 International (CC BY 4.0)",
        min_pos = {x=-31000,y=-10,z=-31000},
        max_pos = {x=31000,y=200,z=31000},
        near_nodes = {"group:tree"}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,
})

